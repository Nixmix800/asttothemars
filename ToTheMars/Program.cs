﻿using System;
using System.Text.RegularExpressions;
using ToTheMars.Models;

namespace ToTheMars
{
    public class Program
    {
        private static string InputRegex =
            @"^(D\[(?<deimos>\d{2}:\d{2},\s*\d{2}:\d{2})\])\s*(P\[(?<phobos>\d{2}:\d{2},\s*\d{2}:\d{2})\])$";
        
        private static string MultiTimestampRegex =
            @"((?<rise>\d{2}:\d{2}),\s*(?<set>\d{2}:\d{2}))";
        
        private static string TimestampRegex =
            @"(?<hour>\d{2}):(?<minute>\d{2})";
        
        private static string timestampFormatString = 
            "\tD[rr:rr, ss:ss] P[rr:rr, ss:ss]\n" +
            "First Timestamp represents the RiseTime; Second one represents SetTime;\n" +
            "D for Deimos; P for Phobos;";
        
        static void Main(string[] args)
        {
            
            
            Console.WriteLine("Hello to the MarsMoonOverlapCalculatorThatIsWellTestedKindOfMaybeNot.\n" +
                              "Please give the timestamps in following format:\n{0}", timestampFormatString);

            while (true)
            {
                Console.Write("Your input:  ");
                var input = Console.ReadLine()?.Trim();
                Console.WriteLine("The Overlap is: {0} [MarsMinutes]", GetMoonOverlap(input));
            }
        }

        public static int GetMoonOverlap(string input)
        {
            var overlap = 0;
            if (input == string.Empty)
            {
                Console.WriteLine("Please give me the data.");
            }
            else
            {
                //input = "D[13:91, 23:05] P[22:05, 24:45]";
                //input = "D[18:55, 04:97] P[10:39, 04:00]";
                
                var inputRegex = new Regex(InputRegex, RegexOptions.Compiled);
                
                var matches = inputRegex.Match(input);
                
                var deimosTimestampsString = matches.Groups["deimos"].Value;
                var phobosTimestampsString = matches.Groups["phobos"].Value;

                if (deimosTimestampsString == string.Empty || phobosTimestampsString == string.Empty)
                {
                    Console.WriteLine("Please follow the following format:\n{0}", timestampFormatString);
                }
                else
                {
                    var riseSetRegex = new Regex(MultiTimestampRegex, RegexOptions.Compiled);
                    
                    var deimosTimestamps = riseSetRegex.Match(deimosTimestampsString);
                    var phobosTimestamps = riseSetRegex.Match(phobosTimestampsString);

                    var hourMinuteRegex = new Regex(TimestampRegex, RegexOptions.Compiled);

                    var deimosRiseTimestampMatch = hourMinuteRegex.Match(deimosTimestamps.Groups["rise"].Value);
                    var deimosSetTimestampMatch = hourMinuteRegex.Match(deimosTimestamps.Groups["set"].Value);

                    var phobosRiseTimestampMatch = hourMinuteRegex.Match(phobosTimestamps.Groups["rise"].Value);
                    var phobosSetTimestampMatch = hourMinuteRegex.Match(phobosTimestamps.Groups["set"].Value);
                    
                    var deimosRiseTimestamp = new MarsTimestamp(
                        int.Parse(deimosRiseTimestampMatch.Groups["hour"].Value),
                        int.Parse(deimosRiseTimestampMatch.Groups["minute"].Value));
                    
                    var deimosSetTimestamp = new MarsTimestamp(
                        int.Parse(deimosSetTimestampMatch.Groups["hour"].Value),
                        int.Parse(deimosSetTimestampMatch.Groups["minute"].Value));

                    var phobosRiseTimestamp = new MarsTimestamp(
                        int.Parse(phobosRiseTimestampMatch.Groups["hour"].Value),
                        int.Parse(phobosRiseTimestampMatch.Groups["minute"].Value));
                    
                    var phobosSetTimestamp = new MarsTimestamp(
                        int.Parse(phobosSetTimestampMatch.Groups["hour"].Value),
                        int.Parse(phobosSetTimestampMatch.Groups["minute"].Value));

                    var deimos = new Deimos(deimosRiseTimestamp, deimosSetTimestamp);
                    var phobos = new Phobos(phobosRiseTimestamp, phobosSetTimestamp);

                    overlap = deimos.GetOverlapWith(phobos);
                }
            }
            
            return overlap;
        }
    }
}