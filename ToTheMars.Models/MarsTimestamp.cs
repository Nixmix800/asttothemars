﻿using System;
using System.Net.Mail;

namespace ToTheMars.Models
{
    public class MarsTimestamp
    {
        public MarsHours Hours { get; set; }
        public MarsMinutes Minutes { get; set; }
        
        public int Relative { get; set; }
        public bool Shifted { get; set; }

        public MarsTimestamp(int hours, int minutes)
        {
            if (hours == 25 && minutes > 0)
            {
                throw new ArgumentOutOfRangeException();
            }
            
            Hours = new MarsHours()
            {
                Hours = hours
            };

            Minutes = new MarsMinutes()
            {
                Minutes = minutes
            };

            Relative = ConvertToMinutes();
        }
        
        public bool IsGreaterThen(MarsTimestamp compareAgainst)
        {
            if (Shifted || compareAgainst.Shifted)
            {
                if (Relative > (compareAgainst.Relative + 2500))
                {
                    return true;
                }

                return false;
            }
            
            if (Relative > compareAgainst.Relative)
            {
                return true;
            }
            return false;
        }
        
        public bool IsSame(MarsTimestamp otherTimestamp)
        {
            if (ConvertToMinutes() == otherTimestamp.ConvertToMinutes())
                return true;
            
            return false;
        }

        private int ConvertToMinutes()
        {
            return Hours.Hours * 100 + Minutes.Minutes;
        }

        public void ShiftToNextDay()
        {
            Relative += 2500;
            Shifted = true;
        }
    }
}