﻿using System;
using System.Text.RegularExpressions;

namespace ToTheMars.Models
{
    public abstract class Moon
    {
        public virtual string Name => "Moon";
        public MarsTimestamp SunRiseTime { get; set; }
        public MarsTimestamp SunSetTime { get; set; }

        protected Moon(MarsTimestamp sunRiseTime, MarsTimestamp sunSetTime)
        {
            SunRiseTime = sunRiseTime;
            SunSetTime = sunSetTime;
            
            if (sunRiseTime.IsGreaterThen(sunSetTime))
            {
                sunSetTime.ShiftToNextDay();
            }
        }
        
        public int GetOverlapWith(Moon otherMoon)
        {
            if (SunRiseTime.IsSame(otherMoon.SunSetTime) || SunSetTime.IsSame(otherMoon.SunRiseTime))
                return 1;
            
            if (SunRiseTime.IsGreaterThen(otherMoon.SunSetTime) && otherMoon.SunRiseTime.IsGreaterThen(SunSetTime))
                return 0;
            
            var start = 0;
            var stop = 0;
            
            if (otherMoon.SunRiseTime.IsGreaterThen(SunRiseTime))
            {
                start = otherMoon.SunRiseTime.Relative;
            }
            else
            {
                start = SunRiseTime.Relative;
            }

            if (SunSetTime.IsGreaterThen(otherMoon.SunSetTime))
            {
                stop = otherMoon.SunSetTime.Relative;
            }
            else
            {
                stop = SunSetTime.Relative;
            }
            
            var overlap = stop - start;
            
            return overlap;
        }
    }
}