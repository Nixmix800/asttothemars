﻿using System;

namespace ToTheMars.Models
{
    public class MarsHours
    {
        private int _hours;
        public int Hours
        {
            get => this._hours;
            set
            {
                if (value > 25 || value < 0)
                {
                    throw new ArgumentOutOfRangeException();
                }
                this._hours = value;
            }
        }
    }
}