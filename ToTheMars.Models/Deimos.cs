﻿namespace ToTheMars.Models
{
    public class Deimos : Moon
    {
        public override string Name => "Deimos";

        public Deimos(MarsTimestamp sunRiseTime, MarsTimestamp sunSetTime) : base(sunRiseTime, sunSetTime)
        {
        }
    }
}