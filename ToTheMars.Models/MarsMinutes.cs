﻿using System;

namespace ToTheMars.Models
{
    public class MarsMinutes
    {
        private int _minutes;
        public int Minutes
        {
            get => this._minutes;
            set
            {
                if (value > 100 || value < 0)
                {
                    throw new ArgumentOutOfRangeException();
                }
                else
                {
                    this._minutes = value;
                }
            }
        }
    }
}