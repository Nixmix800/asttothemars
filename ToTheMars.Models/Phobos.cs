﻿namespace ToTheMars.Models
{
    public class Phobos : Moon
    {
        public override string Name => "Phobos";

        public Phobos(MarsTimestamp sunRiseTime, MarsTimestamp sunSetTime) : base(sunRiseTime, sunSetTime)
        {
        }
    }
}