﻿namespace ToTheMars.Models
{
    public enum MarsMoons
    {
        Deimos,
        Phobos
    }
}